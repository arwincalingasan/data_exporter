<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="latin1">
    <title>Data Exporter</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
</head>

<style>
form.form-horizontal {
    background-color: #ebd6d6;
    padding: 23px;
    height: 80%;
}
</style>

<body>
    <div class="container">

        <!-- Image loader -->
        <div id='loader' style='display: none;'>
            <img src='reload.gif' width='32px' height='32px'>
        </div>
        <!-- Image loader -->

        <h1 class="text-center p-3">First Filter</h1>
        <hr>
        <div class="col-md-12">
            <div class="row">
                <div class="col-sm-6">
                    <h3>Import</h3>
                    <?php 
						if(isset($response)){ 
							echo $response;
						}
					?>
                    <form method='post' class="form-horizontal" action='<?php echo base_url(); ?>data/first_import'
                        enctype="multipart/form-data">
                        <div class="form-group">
                            <input type='file' name='file'>
                            <!-- <label>Enter Region</label>
                            <input type="text" name="range" placeholder="ex. NCR, 1-2"> -->
                            <hr>
                            <input type='submit' value='Upload' name='upload' class="btn btn-primary">
                        </div>
                    </form>
                </div>

                <div class="col-sm-6">
                    <h3>Export</h3>
                    <form method="post" class="form-horizontal" action="<?php echo base_url(); ?>data/first_export"
                        align="left">
                        <select name="exporttype">
                            <option value="pipe">Pipe</option>
                            <option value="comma">Comma</option>
                        </select>
                        <select name="type">
                            <option value="Customer">Customer</option>
                            <option value="Company">Company</option>
                        </select>
                        <hr>
                        <input type="submit" name="export" name="export" value="CSV Export" class="btn btn-success" />
                    </form>
                </div>

            </div>
        </div>
        <div class="col-sm-12">
            <div class="row">
                <h1 class="text-center p-3">Second Filter</h1>
                <hr>
                <div class="col-sm-6">
                    <h3>Import</h3>
                    <?php 
						if(isset($response)){
							echo $response;
						}
					?>
                    <form method='post' class="form-horizontal" action='<?php echo base_url(); ?>data/second_import'
                        enctype="multipart/form-data">
                        <div class="form-group">
                            <input type='file' name='file'>
                            <!-- <label>Enter Region</label>
                            <input type="text" name="range" placeholder="ex. NCR, 1-2"> -->
                            <hr>
                            <input type='submit' value='Upload' name='upload' class="btn btn-primary">
                        </div>
                    </form>
                </div>

                <div class="col-sm-6">
                    <h3>Export</h3>
                    <form method="post" class="form-horizontal" action="<?php echo base_url(); ?>data/second_export"
                        align="left">
                        <select name="exporttype">
                            <option value="pipe">Pipe</option>
                            <option value="comma">Comma</option>
                        </select>
                        <select name="type1">
                            <option value="Customer">Customer</option>
                            <option value="Contact">Contact</option>
                            <option value="Company">Company</option>
                        </select>
                        <hr>
                        <input type="submit" name="export" name="export" value="CSV Export" class="btn btn-success" />
                    </form>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="row">
                <h1 class="text-center p-3">Third Filter (for company only)</h1>
                <hr>
                <div class="col-sm-6">
                    <h3>Import</h3>
                    <?php 
						if(isset($response)){
							echo $response;
						}
					?>
                    <form method='post' class="form-horizontal" action='<?php echo base_url(); ?>data/third_import'
                        enctype="multipart/form-data">
                        <div class="form-group">
                            <input type='file' name='file'>
                            <!-- <label>Enter Region</label>
                            <input type="text" name="range" placeholder="ex. NCR, 1-2"> -->
                            <hr>
                            <input type='submit' value='Upload' name='upload' class="btn btn-primary">
                        </div>
                    </form>
                </div>

                <div class="col-sm-6">
                    <h3>Export</h3>
                    <form method="post" class="form-horizontal" action="<?php echo base_url(); ?>data/third_export"
                        align="left">
                        <select name="exporttype">
                            <option value="pipe">Pipe</option>
                            <option value="comma">Comma</option>
                        </select>
                        <!-- <select name="type1">
                            <option value="Customer">Customer</option>
                            <option value="Contact">Contact</option>
                            <option value="Company">Company</option>
                        </select> -->
                        <hr>
                        <input type="submit" name="export" name="export" value="CSV Export" class="btn btn-success" />
                    </form>
                </div>
            </div>
        </div>

    </div>
    <!-- Option 1: Bootstrap Bundle with Popper -->
    <!-- <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js"></script>					 -->
</body>

</html>
