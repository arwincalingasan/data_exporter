<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') or define('SHOW_DEBUG_BACKTRACE', true);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE') or define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') or define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE') or define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE') or define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ') or define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE') or define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE') or define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE') or define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE') or define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE') or define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT') or define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT') or define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

switch(ENVIRONMENT):
        default:
        date_default_timezone_set('Asia/Manila');
        defined('BASE_URL')          ? null : define('BASE_URL', 'http://localhost/data_exporter/');

        # ERS_DB
        defined('ERS_DB_HOST') ? null : define('ERS_DB_HOST', '172.18.0.22:32775');
        defined('ERS_DB_USER') ? null : define('ERS_DB_USER', 'dev');
        defined('ERS_DB_PASSWORD') ? null : define('ERS_DB_PASSWORD', 'root');
        defined('ERS_DB_NAME') ? null : define('ERS_DB_NAME', 'dev_rms');

        // defined('ERS_DB_PROD_HOST') ? null : define('ERS_DB_PROD_HOST', '');
        // defined('ERS_DB_PROD_USER') ? null : define('ERS_DB_PROD_USER', '');
        // defined('ERS_DB_PROD_PASSWORD') ? null : define('ERS_DB_PROD_PASSWORD', '');
        // defined('ERS_DB_PROD_NAME') ? null : define('ERS_DB_PROD_NAME', '');

        // # PORTAL GLOBAL_1
        // defined('PORTAL_GLOBAL_1_DB_HOST')      ? null : define('PORTAL_GLOBAL_1_DB_HOST', '192.168.200.3');
        // defined('PORTAL_GLOBAL_1_DB_USER')      ? null : define('PORTAL_GLOBAL_1_DB_USER', 'appserver');
        // defined('PORTAL_GLOBAL_1_DB_PASSWORD')  ? null : define('PORTAL_GLOBAL_1_DB_PASSWORD', 'opulently tadpole mulberry ether overfeed drizzly');
        // defined('PORTAL_GLOBAL_1_DB_NAME')      ? null : define('PORTAL_GLOBAL_1_DB_NAME', 'portal_global_1');

        // defined('EMS_DB_HOST')      ? null : define('EMS_DB_HOST', '192.168.200.3');
        // defined('EMS_DB_USER')      ? null : define('EMS_DB_USER', 'appserver');
        // defined('EMS_DB_PASSWORD')  ? null : define('EMS_DB_PASSWORD', 'opulently tadpole mulberry ether overfeed drizzly');
        // defined('EMS_DB_NAME')      ? null : define('EMS_DB_NAME', 'ems_plantilla');

        // defined('RECAPTCHA_SITE_KEY')   ? null : define('RECAPTCHA_SITE_KEY', '6LfXVKsZAAAAAIkD1-AyIouCrO-uiey9uhXBwc_K');
        // defined('RECAPTCHA_SECRET_KEY') ? null : define('RECAPTCHA_SECRET_KEY', '6LfXVKsZAAAAAK1slp1dyJpr11oCtoCC5c6-6AKe');

        // defined('SMS_API_KEY')       ? null : define('SMS_API_KEY', '');    # Enter telerivet API Key
        // defined('SMS_PROJECT_ID')    ? null : define('SMS_PROJECT_ID', ''); # Enter telerivet SMS Project ID

        break;
endswitch;

