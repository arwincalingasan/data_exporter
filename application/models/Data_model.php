<?php
class Data_model extends CI_Model
{
    function fetch_data($operator, $type)
    {
        $this->db->select("*");
        $this->db->from('cust_tbl');

       // $this->db->where(array("lastname $operator"=> NULL, "custName !="=>"", "custName !="=>"-, - -."));


        if($type=="Customer" || $type=="Contact"){
            //$this->db->where(array("title !="=>"Company"));
            //$this->db->where("id BETWEEN 2400001 AND 2700000");
            //$this->db->where("lastname LIKE ", "A%", "lastname LIKE ", "B%", "lastname LIKE ", "C%", );
            $this->db->where("
            title != 'Company'
            AND (
                lastname LIKE 'A%' 
            )
                  
             ");
            $this->db->group_by(array("lastname", "birthdate"));      
        }else{
            $this->db->where(array("title"=>"Company"));
            $this->db->group_by(array("custName","tin"));
        }
      
        $this->db->order_by("id", "asc");   
        return $this->db->get();
    }

    function fetch_data1($operator, $type)
    {

        if($type=="Customer" || $type=="Contact"){
            $title1 = ' title != "Company"';
            $title2 = ' AND title != "Company"';
        }else{
            $title1 = ' title = "Company"';
            $title2 = ' AND title = "Company"';
        }

        $sql = "";

        $sql .= "Select * FROM tblcustomer2
        WHERE (customerName, year) IN 
        (Select customerName, MAX(year) FROM tblcustomer2 WHERE $title1 GROUP by customerName) 
        OR (customerName, year) IN 
        (Select customerName, year WHERE class = 0 $title2)
        GROUP by customerName";

        $sql1 = $this->db->query($sql);
        return $sql1;
    }

    function fetch_company(){
        $sql = "(Select * from tblcustomer3 WHERE CHAR_LENGTH(tax) > 6 GROUP BY tax ORDER by id DESC)
        UNION (Select * from tblcustomer3 WHERE CHAR_LENGTH(tax) <= 6)";
        return $this->db->query($sql);
    }

    function fetch_maur()
    {
        $sql = $this->db->query("Select * from tblcustomer2
        where 
        class <> 0 AND
        (customerName, year)
        IN (Select customerName, MAX(year)
        FROM tblcustomer2 GROUP by customerName)
        UNION
        Select * FROM tblcustomer2 WHERE class = 0
        GROUP by customerName");
    }

    function insertRecord($customer){
        $get = $this->db->insert_batch('tblcustomer2', $customer);
        return $get;
    }

    function insertThirdRecord($customer){
        $get = $this->db->insert_batch('tblcustomer3', $customer);
        return $get;
    }

    function insertTaxZone($customer){
        $get = $this->db->insert_batch('tbl_taxzone', $customer);
        return $get;
        //return $this->db->insert_id();
    }

    function insertInstallment($customer){
        $get = $this->db->insert_batch('tblinstallment', $customer);
        return $get;
    }

    function get_tax_by_customerid($data){
        return $this->db->query("SELECT taxId FROM tbl_taxzone WHERE CustomerId = '".$data."'");
    }

    function check_customerid_if_exist($data){
        return $this->db->query("SELECT cust_no FROM cust_tbl WHERE cust_no = '".$data."'");
    }

    function select_tbl_taxzone(){
        return $this->db->query("SELECT cust_no, taxzone FROM tbl_taxzone");
    }

    function select_cust_tbl(){
        return $this->db->query("SELECT * FROM cust_tbl WHERE id BETWEEN 1 AND 100000");

    }

    function insertRecord5($customer){
        $get = $this->db->insert_batch('cust_tbl2', $customer);
        return $get;
    }

}

?>
