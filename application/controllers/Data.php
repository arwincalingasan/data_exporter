<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Data extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('data_model');
    }

    function index()
    {
        $this->load->view('data_view');
    }

    function first_export()
    {

        $type = $this->input->post('type');
        $exporttype = $this->input->post('exporttype');

        if($type == "Company"){
            $type = "Company";
            $file_name = "Company_". '_' . date('Ymd') . '.csv';
            $operator = "=";

        }else{
            $type = "Customer";
            //$type1 = array("Mr.", "Ms.", "", "A1", "03/16/85");
            $file_name = "Customer_" . date('Ymd') . '.csv'; //should edit
            $operator = "!=";
            $group = '"lname", "birthday"';
        }

        $customer_data = $this->data_model->fetch_data($operator, $type)->result_array();

        header("Content-Description: File Transfer");
        header("Content-Disposition: attachment; filename=$file_name");
        header("Content-Type: application/csv;");

        $file = fopen('php://output', 'w');

        if ($type == "Customer")
        {
            $header = array( 
                "id", "cust_no", "custName", "firstname", "mi", "lastname", "suffix", "branch_code", "branch_name", "street", "brgy", "city", "province", "district", "zip", "region", "tin", "title", "birthdate", "contact",
                 "email", "maur", "occupation"

            );
            fputcsv($file, $header);
            $array = [];
            $arr = [];
  
            foreach ($customer_data as $value)
            {
                $region = $this->get_region($value['region']);

                $arr = array(
                    $array = array(
                        "1" =>  "",
                        "2" => $value["cust_no"],
                        "3" => $value["custName"],
                        "4" => $value["firstname"],
                        "5" => $value["mi"],
                        "6" => $value["lastname"],
                        "7" => "",
                        "8" => "",
                        "9" => "",
                        "10" => $value["street"],
                        "11" => "",
                        "12" => $value["city"],
                        "13" => "",
                        "14" => "", //fax
                        "15" => "", //fax
                        "16" => $value["region"], //acct ref
                        "17" => $value['tin'], //fax
                        "18" => $value["title"],
                        "19" => $value["birthdate"],
                        "20" => "",
                        "21" => "", //  
                        "22" => $value['maur'], //
                        "23" => $value['occupation'], //
                    )
                );

                foreach ($arr as $key => $value)
                {
                    if($exporttype == "pipe"){
                        fputcsv($file, $value, '|');
                    }else{
                        fputcsv($file, $value);
                    } 
                }
            }
        }
        else{

            $header = array( 
                "id", "cust_no", "custName", "firstname", "mi", "lastname", "suffix", "branch_code", "branch_name", "street", "brgy", "city", "province", "district", "zip", "region", "tin", "title", "birthdate", "contact",
                 "email", "maur"

            );
            fputcsv($file, $header);
            $array = [];
            $arr = [];
  
            foreach ($customer_data as $value)
            {
                $region = $this->get_region($value['region']);

                $arr = array(
                    $array = array(
                        "1" =>  "",
                        "2" => $value["cust_no"],
                        "3" => $value["custName"],
                        "4" => "",
                        "5" => "",
                        "6" => "",
                        "7" => "",
                        "8" => "",
                        "9" => "",
                        "10" => $value["street"],
                        "11" => "",
                        "12" => $value["city"],
                        "13" => "",
                        "14" => "", //fax
                        "15" => "", //fax
                        "16" => $value["region"], //acct ref
                        "17" => $value["tin"],
                        "18" => $value["title"],
                        "19" => "",
                        "20" => "",
                        "21" => "", //  
                        "22" => $value['maur'], //
                        "23" => "", //
                    )
                );

                foreach ($arr as $key => $value)
                {
                    if($exporttype == "pipe"){
                        fputcsv($file, $value, '|');
                    }else{
                        fputcsv($file, $value);
                    } 
                }
            }

        }
    }

    function second_import()
    {

        if ($this->input->post('upload') != NULL)
        {

            $data = array();

            if (!empty($_FILES['file']['name']))
            {
                // Set preference
                $config['upload_path'] = 'assets/files';
                $config['allowed_types'] = 'csv';
                $config['max_size'] = '900000'; // max_size in kb
                $config['file_name'] = $_FILES['file']['name'];

                // Load upload library
                $this->load->library('upload', $config);

                // File upload
                if ($this->upload->do_upload('file'))
                {

                
                    // Get data about the file
                    $uploadData = $this->upload->data();
                    $filename = $uploadData['file_name'];

                    // Reading file
                    $file = fopen("assets/files/" . $filename, "r");
                    $i = 0;
                    $numberOfFields = 23; // Total number of fields
                    $importData_arr = array(); 

                    while (($filedata = fgetcsv($file, 900000, ",")) !== false)
                    {
                        $num = count($filedata); 
                            for ($c = 0;$c < $num;$c++)
                            {
                                $importData_arr[$i][] = $filedata[$c];
                            }
                        //}
                        $i++;
                    }
                    fclose($file);

                    $skip = 0;

                    // insert import data
                    foreach ($importData_arr as $record)
                    {
                        // Skip first row
                        if ($skip != 0)
                        {
                            if(count($record) > 0){       
                
                                $class = substr($record[1], -4, 1); 
                                $getYear1 = substr($record[1], 4, -4);
                                $getSequence = substr($record[1], -3, 7);

                                if($getYear1 >  21 ){
                                    $getYear = '19' . $getYear1. $getSequence;
                                }else{
                                    $getYear = '20' . $getYear1 . $getSequence;
                                }

                                if(empty($record[21])){
                                    $maur = "";
                                }else{
                                    $maur = $record[21];
                                }

                                if(empty($record[22])){
                                    $occupation = "";
                                }else{
                                    $occupation = $record[22];
                                }

                                $newcustomer = array(  
                                    array(
                                        "customerId" => $record[1],
                                        "customerName" => ltrim($record[2]),
                                        "fname" => $record[3],
                                        "mname" => $record[4],
                                        "lname" => ltrim($record[5]),
                                        "address1" => $record[9],
                                        "city" => $record[11],
                                        "region" => $record[15],
                                        "class" => $class,
                                        "birthday" => $record[18],
                                        "title" => $record[17],
                                        "tax" => $record[16],
                                        "year" => $getYear,
                                        "maur" => $maur,
                                        "occupation" => $occupation,
                                        //"sequence" => $getSequence,
                                    )
                                );

                                $response = $this->data_model->insertRecord($newcustomer);  

                                // $newInstallment = array(  
                                //     array(
                                //         "CustomerId" => $record[1],
                                //         "Maur" => substr($record[1], -4, 1),
                                //         "AcuId" => $response,
                                //     )
                                // );

                                // $response= $this->data_model->insertInstallment($newInstallment);  
                            } 
                        }
                        $skip++;
                    }
                    $data['response'] = 'successfully uploaded ' . $filename;
                }
                else
                {
                    $data['response'] = 'failed';
                }
            }
            else
            {
                $data['response'] = 'failed';
            }
            // load view
            $this->index();
        }
        else
        {
            // load view
            $this->index();
        }
    }

    function second_export()
    {

        $type = $this->input->post('type1');
        $exporttype = $this->input->post('exporttype');

        if($type == "Company"){
            $type = "Company";
            $file_name = "Company_". '_' . date('Ymd') . '.csv';
            $operator = "=";

        }else if($type == "Contact"){
            $type = "Contact";
            //$type1 = array("Mr.", "Ms.", "", "A1", "03/16/85"); //should edit
            $file_name = "Contact_". '_' . date('Ymd') . '.csv';
            $operator = "!=";
        }else if($type == "Customer"){
            $type = "Customer";
            //$type1 = array("Mr.", "Ms.", "", "A1", "03/16/85"); //should edit
            $file_name = "Customer_". '_' . date('Ymd') . '.csv';
            $operator = "!=";
        }else if($type == "MAUR"){
            $type = "Customer";
            //$type1 = array("Mr.", "Ms.", "", "A1", "03/16/85");
            $file_name = "Customer_" . date('Ymd') . '.csv'; //should edit
            $operator = "!=";
            $group = '"lname", "birthday"';
        }

        $customer_data = $this->data_model->fetch_data1($operator, $type)->result_array();
        //$customer_data = $this->data_model->fetch_data1()->result_array();

        header("Content-Description: File Transfer");
        header("Content-Disposition: attachment; filename=$file_name");
        header("Content-Type: application/csv;");

        $file = fopen('php://output', 'w');

        if ($type == "Customer")
        {
            $header = array( "Customer ID", "Customer Name", "Status", "Company Name", "Attention", "Email", "Phone 1", 
            "Phone 2", "Fax", "Ext Ref Nbr", "Address Line 1", "Address Line 2", "Barangay", "City", "State/Province", "Region", 
            "Country", "PSGC", "Postal Code", "Customer Class", "Terms", "Statement Cycle ID", "Auto-Apply Payments", 
            "Apply Overdue Charges", "Enable Write-offs", "Write-off Limit", "Currency ID", "Enable Currency Override", 
            "Curr. Rate Type", "Enable Rate Override", "Credit Verification", "Consented to the Processing of Personal Data", 
            "Date of Consent", "Consent Expires", "Sames as Main", "Company Name", "Attention", "Phone 1", "Phone 2", "Fax", "Email", 
            "Web", "Address Line 1", "Address Line 2", "Baranggay", "City", "State/Province", "Region", "Country", "PSGC", 
            "Postal Code", "Allow Appointment Notification", "Require Customer Signature Mobile App", "Default Billing Customer", 
            "Parent Account", "Consolidate Balance", "Consolidate Statements", "Share Credit Policy", "Send Invoice by Email", 
            "Send Dunning Letters by Email", "Send Statements by Email", "Print Invoices", "Print Dunning Letters", "Print Statements", 
            "Statement Type", "Multi-Currency Statements", "Same as Main", "Company Name", "Attention", "Email", "Web", "Phone 1", 
            "Phone 2", "Fax", "Same as Main", "Address Line 1", "Address Line 2", "Baranggay", "City", "State/Province", "Region", 
            "Country", "PSG", "Postal Code", "Location Name", "Tax Registration ID", "Tax Zone", "Entity Usage Type", "Shipping Branch", 
            "Price Class", "Warehouse", "Ship Via", "Shipping Terms", "Shipping Zone ID", "FOB Point", "Resident Delivery", 
            "Saturday Delivery", "Insurance", "Shipping Rule", "Order Priority", "Lead Time (days)", "AR Account", "AR Sub", 
            "Sales Account", "Sales Sub", "Discount Sub", "Freight Account", "Freight Sub", "Cash Discount Account", "Cash Discount Sub", 
            "Prepayment Account", "Prepayment Sub"
            );
            fputcsv($file, $header);
            $array = [];
            $arr = [];
  
            foreach ($customer_data as $value)
            {

                $region = $this->get_region($value['region']);

               // $tax = $this->data_model->get_tax_by_customerid($value['customerId']);

                $arr = array(
                    $array = array(
                        "id" =>  "",
                        "customerName" => $value["customerName"],
                        "status" => "ACTIVE",
                        "blank1" => "", //company name
                        "blank2" => "",
                        "blank3" => "",
                        "blank4" => "",
                        "blank5" => "",
                        "blank6" => "", //fax
                        "blank7" => $value["customerId"],
                        "address1" => $value["address1"],
                        "blank8" => "", //acct ref
                        "blank9" => "", //acct ref
                        "city" => $value["city"],
                        "blank10" => "", //acct ref
                        "region" => $region,
                        "country" => $value["country"],
                        "blank11" => "", // PSG
                        "blank12" => "", // postal code
                        "class" => $value["class"],
                        "blank12" => "", //
                        "blank13" => "", //
                        "blank14" => "", //
                        "blank15" => "", //
                        "blank16" => "", //
                        "blank17" => "", //
                        "blank18" => "", //
                        "blank19" => "", //
                        "blank20" => "", //
                        "blank21" => "", //
                        "blank22" => "", //
                        "blank23" => "", //
                        "blank24" => "", //
                        "blank25" => "", //
                        "blank26" => "", //
                        "blank27" => "", //
                        "blank28" => "", //
                        "blank30" => "", //
                        "blank31" => "", //
                        "blank32" => "", //
                        "blank33" => "", //
                        "blank34" => "", //            
                        "blank35" => "", // ito
                        "address2" => $value["address1"], // 43
                        "blank36" => "", //           
                        "blank37" => "", //
                        "city2" => $value["city"], // ito   
                        "blank38" => "", //
                        "region2" => $region, // ito
                        "country2" => $value["country"], // ito
                        "blank39" => "", //
                        "blank40" => "", //
                        "blank41" => "", //
                        "blank42" => "", //
                        "blank43" => "", //
                        "blank44" => "", //
                        "blank45" => "", //
                        "blank46" => "", //
                        "blank47" => "", //
                        "blank48" => "", //
                        "blank49" => "", //
                        "blank50" => "", //
                        "blank51" => "", //
                        "blank52" => "", //
                        "blank53" => "", //
                        "blank54" => "", //
                        "blank55" => "", //
                        "blank56" => "", //
                        "blank57" => "", //
                        "blank58" => "", //
                        "blank59" => "", //
                        "blank60" => "", //
                        "blank61" => "", //
                        "blank62" => "", //
                        "blank63" => "", //
                        "blank64" => "", //
                        "address3" => $value["address1"], // 76
                        "blank65" => "", //
                        "blank66" => "", //
                        "city3" => $value["city"], // 79
                        "blank" => "", //
                        "region3" => $region, // 79
                        "country3" => $value["country"], // 79
                        "blank106" => "", //
                        "blank107" => "", //
                        "blank108" => "", //
                        "blank109" => $value['tax'], //
                        "blank150" => "", //
                        "blank" => "", //
                        "blank" => "", //
                        "blank" => "", //
                        "blank" => "", //
                        "blank" => "", //
                        "blank" => "", //
                        "blank" => "", //
                        "blank" => "", //
                        "blank" => "", //
                        "blank" => "", //
                        "blank" => "", //
                        "blank" => "", //
                        "blank" => "", //
                        "blank" => "", //
                        "blank" => "", //
                        "blank" => "", //
                        "blank" => "", //
                        "blank" => "", //
                        "blank" => "", //
                        "blank" => "", //
                        "blank" => "", //
                        "blank" => "", //
                        "blank" => "", //
                        "blank" => "", //
                        "blank" => "", //
                        "blank" => "", //
                        "blank" => "", //
                        "blank" => "", //
                        "blank" => "", //
                        "blank" => "", //
                        "blank" => "", //
                        
                    )
                );

                foreach ($arr as $key => $value)
                {
                    if($exporttype == "pipe"){
                        fputcsv($file, $value, '|');
                    }else{
                        fputcsv($file, $value);
                    } 
                }
         }

        }
        else if ($type == "Contact")
        {

            $header = array(
                "Contact ID", "First Name", "Title", "Last Name", "Middle Name", "Active", "Business Account", "Owner", "Workgroup", "Job Title", "Occupation", 
                "Contact Class", "PWD", "Senior Citizen", "Zero Rated", "Company Name", "Email", "Web", "Business 1", "Cell", "Home", "Business Fax", "Address Line 1", 
                "Address Line 2", "Baranggay", "City", "State/Province", "Region", "Country", "PSGC", "Postal Code", "Contact Method", "Do Not Call", "Do Not Email", 
                "No Mass Mail", "Do Not Fax", "Do Not Mail", "No Marketing", "Parent Account", "Birthday", "Gender", "Marital Status", "Spouse/Partner Name", "Ext Ref Nbr"
            );
            fputcsv($file, $header);
            $array = [];
            $arr = [];
            foreach ($customer_data as $value)
            {

                $region = $this->get_region($value['region']);

                if(preg_match('/[a-z]/i', $value['birthday'])){
                    $bdate = '';
                }else if(empty($value['birthday'])){
                    $bdate = '';
                }else{
                    $bdate = $value['birthday'];
                }

                $title = "";
                if($value['title'] == "Mr."){
                  $gender = "M";
                }else if($value['title'] == "Ms."){
                 $gender = "F";
                }

                $arr = array(
                    $array = array(
                        "id5" => "",
                        "fname" => $value["fname"],
                        "title" => $value["title"],
                        "lname" => $value['lname'],
                        "mname" => $value['mname'],
                        "blank3" => "TRUE",
                        //"id" =>  '0'. sprintf('%07d',$value["id"]),
                        "id" => "",
                        "blank5" => "",
                        "blank6" => "",
                        "blank7" => "",
                        "blank8" => $value['occupation'],
                        "blank9" => "",
                        "blank10" => "",
                        "blank11" => "",
                        "blank12" => "",
                        "blank13" => "", //
                        "blank14" => "", //
                        "blank15" => "", //
                        "blank16" => "", //
                        "blank17" => "", //
                        "blank18" => "", //
                        "blank19" => "", // 
                        "address1" => $value["address1"],
                        "blank21" => "", //                 
                        "blank22" => "",
                        "city" => $value["city"],             
                        "blank23" => "", //
                        "region" => $region,
                        "country" => $value["country"],                 
                        "blank20" => "", //             
                        "blank24" => "", //
                        "blank25" => "", //
                        "blank26" => "", //
                        "blank27" => "", //
                        "blank28" => "", //
                        "blank30" => "", //
                        "blank31" => "", //
                        "blank32" => "", //
                        "blank33" => "", //
                        "birthday" => $bdate,
                        "blank38" => $gender, //
                        "blank34" => "", //  
                        "blank39" => "", //
                        "blank41" => $value["customerId"],
                        
                    )
                );

                foreach ($arr as $key => $value)
                {
                    if($exporttype == "pipe"){
                        fputcsv($file, $value, '|');
                    }else{
                        fputcsv($file, $value);
                    } 
                }
            }

        }
        else if($type == "Company")
        {

            $header = array( 
                "id", "cust_no", "custName", "firstname", "mi", "lastname", "suffix", "branch_code", "branch_name", "street", "brgy", "city", "province", "district", "zip", "region", "tin", "title", "birthdate", "contact",
                 "email", "maur"

            );
            fputcsv($file, $header);
            $array = [];
            $arr = [];

           // $company_data = $this->data_model->fetch_company()->result_array();
  
            foreach ($customer_data as $value)
            {
                $region = $this->get_region($value['region']);

                $arr = array(
                    $array = array(
                        "1" =>  "",
                        "2" => $value["customerId"],
                        "3" => $value["customerName"],
                        "4" => "",
                        "5" => "",
                        "6" => "",
                        "7" => "",
                        "8" => "",
                        "9" => "",
                        "10" => $value["address1"],
                        "11" => "",
                        "12" => $value["city"],
                        "13" => "",
                        "14" => "", //fax
                        "15" => "", //fax
                        "16" => $value["region"], //acct ref
                        "17" => $value["tax"],
                        "18" => $value["title"],
                        "19" => "",
                        "20" => "",
                        "21" => "", //  
                        "22" => $value['maur'], //
                    )
                );

                foreach ($arr as $key => $value)
                {
                    if($exporttype == "pipe"){
                        fputcsv($file, $value, '|');
                    }else{
                        fputcsv($file, $value);
                    } 
                }
            }
        }
    }

    function third_import()
    {
        $reg_num = $this->input->post('range');

        if ($this->input->post('upload') != NULL)
        {
            $data = array();

            if (!empty($_FILES['file']['name']))
            {
                // Set preference
                $config['upload_path'] = 'assets/files';
                $config['allowed_types'] = 'csv';
                $config['max_size'] = '900000'; // max_size in kb
                $config['file_name'] = $_FILES['file']['name'];

                // Load upload library
                $this->load->library('upload', $config);

                // File upload
                if ($this->upload->do_upload('file'))
                {
                
                    // Get data about the file
                    $uploadData = $this->upload->data();
                    $filename = $uploadData['file_name'];

                    // Reading file
                    $file = fopen("assets/files/" . $filename, "r");
                    $i = 0;
                    $numberOfFields = 22; // Total number of fields
                    $importData_arr = array(); 

                    while (($filedata = fgetcsv($file, 900000, ",")) !== false)
                    {
                        $num = count($filedata); 
                            for ($c = 0;$c < $num;$c++)
                            {
                                $importData_arr[$i][] = $filedata[$c];
                            }
                        //}
                        $i++;
                    }
                    fclose($file);

                    $skip = 0;

                    // insert import data
                    foreach ($importData_arr as $record)
                    {
                        // Skip first row
                        if ($skip != 0)
                        {
                            if(count($record) > 0){       
                
                                $class = substr($record[1], -4, 1); 
                                $getYear1 = substr($record[1], 4, -4);
                                $getSequence = substr($record[1], -3, 7);

                                if($getYear1 >  21 ){
                                    $getYear = '19' . $getYear1. $getSequence;
                                }else{
                                    $getYear = '20' . $getYear1 . $getSequence;
                                }

                                $newcustomer = array(  
                                    array(
                                        "customerId" => $record[1],
                                        "customerName" => ltrim($record[2]),
                                        "fname" => $record[3],
                                        "mname" => $record[4],
                                        "lname" => ltrim($record[5]),
                                        "address1" => $record[9],
                                        "city" => $record[11],
                                        "region" => $record[15],
                                        "class" => $class,
                                        "birthday" => $record[18],
                                        "title" => $record[17],
                                        "tax" => $record[16],
                                        "year" => $getYear,
                                        "maur" => $record['21']
                                    )
                                );

                                $response = $this->data_model->insertThirdRecord($newcustomer);  

                                // $newInstallment = array(  
                                //     array(
                                //         "CustomerId" => $record[1],
                                //         "Maur" => substr($record[1], -4, 1),
                                //         "AcuId" => $response,
                                //     )
                                // );

                                //$response= $this->data_model->insertInstallment($newInstallment);  
                            } 
                        }
                        $skip++;
                    }
                    $data['response'] = 'successfully uploaded ' . $filename;
                }
                else
                {
                    $data['response'] = 'failed';
                }
            }
            else
            {
                $data['response'] = 'failed';
            }
            // load view
            $this->index();
        }
        else
        {
            // load view
            $this->index();
        }
    }

    function third_export()
    {

        $type = $this->input->post('type1');
        $exporttype = $this->input->post('exporttype');

            $type = "Company";
            $file_name = "Company_". '_' . date('Ymd') . '.csv';
            $operator = "=";


        $customer_data = $this->data_model->fetch_company()->result_array();
        //$customer_data = $this->data_model->fetch_data1()->result_array();

        header("Content-Description: File Transfer");
        header("Content-Disposition: attachment; filename=$file_name");
        header("Content-Type: application/csv;");

        $file = fopen('php://output', 'w');

            $header = array("Customer ID","Customer Name","Status","Company Name","Attention","Email","Phone 1","Phone 2","Fax",
            "Ext Ref Nbr","Address Line 1","Address Line 2","Barangay","City","State/Province","Region","Country","PSGC","Postal Code",
            "Customer Class","Terms","Statement Cycle ID","Auto-Apply Payments","Apply Overdue Charges","Enable Write-offs","Write-off Limit",
            "Currency ID","Enable Currency Override","Curr. Rate Type","Enable Rate Override","Credit Verification",
            "Consented to the Processing of Personal Data","Date of Consent","Consent Expires","Sames as Main","Company Name","Attention",
            "Phone 1","Phone 2","Fax","Email","Web","Address Line 1","Address Line 2","Baranggay","City","State/Province","Region","Country",
            "PSGC","Postal Code","Allow Appointment Notification","Require Customer Signature Mobile App","Default Billing Customer",
            "Parent Account","Consolidate Balance","Consolidate Statements","Share Credit Policy","Send Invoice by Email",
            "Send Dunning Letters by Email","Send Statements by Email","Print Invoices","Print Dunning Letters","Print Statements",
            "Statement Type","Multi-Currency Statements","Same as Main","Company Name","Attention","Email","Web","Phone 1","Phone 2",
            "Fax","Same as Main","Address Line 1","Address Line 2","Baranggay","City","State/Province","Region","Country","PSGC","Postal Code",
            "Location Name","Tax Registration ID","Tax Zone","Entity Usage Type","Shipping Branch","Price Class","Warehouse","Ship Via",
            "Shipping Terms","Shipping Zone ID","FOB Point","Resident Delivery","Saturday Delivery","Insurance","Shipping Rule","Order Priority",
            "Lead Time (days)","AR Account","AR Sub","Sales Account","Sales Sub","Discount Sub","Freight Account","Freight Sub",
            "Cash Discount Account","Cash Discount Sub","Prepayment Account","Prepayment Sub"
            );
            fputcsv($file, $header);
            $array = [];
            $arr = [];
            
            foreach ($customer_data as $value)
            {

                if(strlen($value['tax'] > 7)){
                    $tax = $value['tax'];
                }else{
                    $tax = "";
                }

                $region = $this->get_region($value['region']);

                //$taxzone = $this->data_model->get_tax_by_customerid($value['customerId']);

                $arr = array(
                    $array = array(
                        "id" =>  "",
                        "fname" => $value["customerName"],
                        "title" => "ACTIVE",
                        "customerId" => "",
                        "blank4" => "",
                        "blank5" => "",
                        "blank6" => "",
                        "blank7" => "",
                        "blank8" => "",
                        "blank9" => $value["customerId"],
                        "address1" => $value["address1"],
                        "blank10" => "",
                        "blank11" => "",
                        "city" => $value["city"],
                        "a" => "",
                        "region" => $region,
                        "country" => $value["country"],
                        "blank12" => "",
                        "blank13" => "", //
                        "class" => $value["class"],
                        "blank14" => "", //
                        "blank15" => "", //
                        "blank16" => "", //
                        "blank17" => "", //
                        "blank18" => "", //
                        "blank19" => "", //
                        "blank20" => "", //
                        "blank21" => "", //
                        "blank23" => "", //
                        "blank24" => "", //
                        "blank25" => "", //
                        "blank26" => "", //
                        "blank27" => "", //
                        "blank28" => "", //
                        "blank30" => "", //
                        "blank31" => "", //
                        "blank32" => "", //
                        "blank33" => "", //
                        "blank34" => "", //
                        "blank22" => "", //
                        "blank38" => "", //
                        "blank39" => "", //
                        "address2" => $value["address1"],
                        "blank40" => "", //
                        "blank41" => "", //
                        "city2" => $value["city"],
                        "blank42" => "", //
                        "region2" => $region,
                        "country2" => $value["country"],
                        "blank43" => "", //
                        "blank44" => "", //
                        "blank45" => "", //
                        "blank46" => "", //
                        "blank47" => "", //
                        "blank48" => "", //
                        "blank49" => "", //
                        "blank50" => "", //
                        "blank51" => "", //
                        "blank52" => "", //
                        "blank53" => "", //
                        "blank54" => "", //
                        "blank55" => "", //
                        "blank56" => "", //
                        "blank57" => "", //
                        "blank58" => "", //
                        "blank59" => "", //
                        "blank60" => "", //
                        "blank61" => "", //
                        "blank62" => "", //
                        "blank63" => "", //
                        "blank95" => "", //
                        "blank96" => "", //
                        "blank97" => "", //
                        "blank98" => "", //
                        "blank99" => "", //
                        "address3" => $value["address1"],
                        "blank100" => "", //
                        "blank64" => "", //
                        "city3" => $value["city"],
                        "blank65" => "", //
                        "region3" => $region,
                        "country3" => $value["country"],
                        "blank66" => "", //           
                        "blank94" => "", //
                        "blank67" => "", //
                        "tax" => $tax,
                        "blank68" => "", //
                        "blank69" => "", //
                        "blank70" => "", //
                        "blank71" => "", //         
                        "blank72" => "", //
                        "blank73" => "", //
                        "blank74" => "", //
                        "blank75" => "", //
                        "blank76" => "", //
                        "blank77" => "", //
                        "blank78" => "", //
                        "blank79" => "", //
                        "blank80" => "", //
                        "blank81" => "", //
                        "blank82" => "", //
                        "blank83" => "", //
                        "blank84" => "", //
                        "blank85" => "", //
                        "blank86" => "", //
                        "blank87" => "", //
                        "blank88" => "", //
                        "blank89" => "", //
                        "blank90" => "", //
                        "blank91" => "", //
                        "blank92" => "", //
                        "blank93" => "", //
                        
                    )
                );

                foreach ($arr as $key => $value)
                {
                    if($exporttype == "pipe"){
                        fputcsv($file, $value, '|');
                    }else{
                        fputcsv($file, $value);
                    } 
                }
            }

        fclose($file);
        exit;
    }

    function get_region($pass_region)
    {
        if($pass_region == "Ilocos"){
            $region = "Region I (Ilocos Region)";
        }else if($pass_region == "Cagayan Valley"){
            $region = "Region II (Cagayan Valley)";
        }else if($pass_region == "Central Luzon"){
            $region = "Region III (Central Luzon)";  
        }else if($pass_region == "South Luzon"){
            $region = "Region IV-A (CALABARZON)";
        }else if($pass_region == "Bicol"){
            $region = "Region V (Bicol Region)";
        }else if($pass_region == "West Visayas"){
            $region = "Region VI (Western Visayas)";
        }else if($pass_region == "Central Visayas"){
            $region = "Region VII (Central Visayas)";
        }else if($pass_region == "Eastern Visayas"){
            $region = "Region VIII (Eastern Visayas)";
        }else if($pass_region == "Western Mindanao"){
            $region = "Region IX (Zamboanga Peninsula)";
        }else if($pass_region == "Northern Mindanao"){
            $region = "Region X (Northern Mindanao)";
        }else if($pass_region == "Central Mindanao"){
            $region = "Region XI (Davao Region)";
        }else if($pass_region == "South Mindanao"){
            $region = "Region XII (SOCCSKSARGEN)";
        }else if($pass_region == "Caraga"){
            $region = "Region XIII (Caraga)";
        }else if($pass_region == "CORDILLERA ADM. REG."){
            $region = "Cordillera Administrative Region (CAR)";
        }else if($pass_region == "AUTO. REG. OF MUSLIM"){
            $region = "Autonomous Region in Muslim Mindanao (ARMM)";
        }else if($pass_region == "National Cap. Region"){
            $region = "National Capital Region (NCR)";
        }else{
            $region = "";
        }

        return $region;
    }  

    function import_customer_tax_zone()
    {
    
        if ($this->input->post('upload') != NULL)
        {
            $data = array();

            if (!empty($_FILES['file']['name']))
            {
  
                // Set preference
                $config['upload_path'] = 'assets/files';
                $config['allowed_types'] = 'csv';
                $config['max_size'] = '100000'; // max_size in kb
                $config['file_name'] = $_FILES['file']['name'];


                // print_r($config);
                // die();
                // Load upload library
                $this->load->library('upload', $config);

                // File upload
                if ($this->upload->do_upload('file'))
                {

                    
                    
                    // Get data about the file
                    $uploadData = $this->upload->data();
                    $filename = $uploadData['file_name'];

                    // Reading file
                    $file = fopen("assets/files/" . $filename, "r");
                    $i = 0;
                    $numberOfFields = 3; // Total number of fields
                    $importData_arr = array();

                    while (($filedata = fgetcsv($file, 1000000, ",")) !== false)
                    {
                        $num = count($filedata);

                        if ($numberOfFields == $num)
                        {
                            for ($c = 0;$c < $num;$c++)
                            {
                                $importData_arr[$i][] = $filedata[$c];
                            }
                        }
                        $i++;
                    }
                    fclose($file);

                    $skip = 0;

                    foreach ($importData_arr as $record)
                    {
                        // Skip first row
                        if ($skip != 0)
                        {
                            if(count($record) > 0){       
                
                                //$this
                                    
                                $newcustomer = array(  
                                    array(
                                        "CustomerId" => $record[0],
                                        "taxId" => $record[1],
                                    )
                                );

                                $response = $this->data_model->insertTaxZone($newcustomer);  
                            } 
                        }
                        $skip++;
                    }
                    $data['response'] = 'successfully uploaded ' . $filename;
                }
                else
                {
                    $data['response'] = 'failed';
                }
            }
            else
            {
                $data['response'] = 'failed';
            }
            // load view
            $this->index();
        }
        else
        {
            // load view
            $this->index();
        }
    }

    function validateDate($date, $format = 'Y-m-d'){
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) === $date;
    }

    function merge_tables(){

        $data = array();
        $result = array();
        $taxzone_array = $this->data_model->select_tbl_taxzone();
        $cust_array = $this->data_model->cust_array();

        foreach($taxzone_array as $key => &$value){
            $result[$key] = array_merge($value, $cust_array[$key]);
        }

        $data['data'] = $result;


    }

}
