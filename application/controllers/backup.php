function selectRecord(){
        $this->db->select("DISTINCT lname, birthday");
        $this->db->from('tblcustomer');
        return $this->db->get();
    }



    <?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Data extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('data_model');
    }

    function index()
    {
        $data['customer_data'] = $this->data_model->fetch_count();
        $this->load->view('data_view', $data);
    }

    function export()
    {

        $type = $this->input->post('type');
        $exporttype = $this->input->post('exporttype');

        if($type == "Company"){
            $type1 = "Company";
            $file_name = "Company_". '_' . date('Ymd') . '.csv';

        }else if($type == "Contact"){
            $type = "Contact";
            $type1 = array("Mr.", "Ms.", "", "A1", "03/16/85"); //should edit
            $file_name = "Contact_". '_' . date('Ymd') . '.csv';
        }else{
            $type = "Customer";
            $type1 = array("Mr.", "Ms.", "", "A1", "03/16/85");
            $file_name = "Customer_" . date('Ymd') . '.csv'; //should edit

        }


        header("Content-Description: File Transfer");
        header("Content-Disposition: attachment; filename=$file_name");
        header("Content-Type: application/csv;");

        $customer_data = $this->data_model->fetch_data($type1);

        $file = fopen('php://output', 'w');

        if ($type == "Customer")
        {
            $header = array( "Field Name", "Customer ID", "Customer Name", "Status", "Company Name", "Attention", "Email", "Phone 1", 
            "Phone 2", "Fax", "Account Ref #", "Address Line 1", "Address Line 2", "Barangay", "City", "State/Province", "Region", 
            "Country", "PSGC", "Postal Code", "Customer Class", "Terms", "Statement Cycle ID", "Auto-Apply Payments", 
            "Apply Overdue Charges", "Enable Write-offs", "Write-off Limit", "Currency ID", "Enable Currency Override", 
            "Curr. Rate Type", "Enable Rate Override", "Credit Verification", "Consented to the Processing of Personal Data", 
            "Date of Consent", "Consent Expires", "Sames as Main", "Company Name", "Attention", "Phone 1", "Phone 2", "Fax	Email", 
            "Web", "Address Line 1", "Address Line 2", "Baranggay", "City", "State/Province", "Region", "Country", "PSGC", 
            "Postal Code", "Allow Appointment Notification", "Require Customer Signature Mobile App", "Default Billing Customer", 
            "Parent Account", "Consolidate Balance", "Consolidate Statements", "Share Credit Policy", "Send Invoice by Email", 
            "Send Dunning Letters by Email", "Send Statements by Email", "Print Invoices", "Print Dunning Letters", "Print Statements", 
            "Statement Type", "Multi-Currency Statements", "Same as Main", "Company Name", "Attention", "Email", "Web", "Phone 1", 
            "Phone 2", "Fax", "Same as Main", "Address Line 1", "Address Line 2", "Baranggay", "City", "State/Province", "Region", 
            "Country", "PSG", "Postal Code", "Location Name", "Tax Registration ID", "Tax Zone", "Entity Usage Type", "Shipping Branch", 
            "Price Class", "Warehouse", "Ship Via", "Shipping Terms", "Shipping Zone ID", "FOB Point", "Resident Delivery", 
            "Saturday Delivery", "Insurance", "Shipping Rule", "Order Priority", "Lead Time (days)", "AR Account", "AR Sub", 
            "Sales Account", "Sales Sub", "Discount Sub", "Freight Account", "Freight Sub", "Cash Discount Account", "Cash Discount Sub", 
            "Prepayment Account", "Prepayment Sub"
            );
            fputcsv($file, $header);
            $array = [];
            $arr = [];
            
            foreach ($customer_data->result_array() as $value)
            {
                $arr = array(
                    $array = array(
                        "blank" =>"",
                        "id" =>  $value["id"],
                        "customerName" => $value["customerName"],
                        "status" => $value["status"],
                        "blank1" => "", //company name
                        "blank2" => "",
                        "blank3" => "",
                        "blank4" => "",
                        "blank5" => "",
                        "blank6" => "", //fax
                        "blank7" => "", //acct ref
                        "address1" => $value["address1"],
                        "blank8" => "", //acct ref
                        "blank9" => "", //acct ref
                        "city" => $value["city"],
                        "blank10" => "", //acct ref
                        "region" => $value["region"],
                        "country" => $value["country"],
                        "blank11" => "", // PSG
                        "blank12" => "", // postal code
                        "class" => $value["class"],
                        "blank12" => "", //
                        "blank13" => "", //
                        "blank14" => "", //
                        "blank15" => "", //
                        "blank16" => "", //
                        "blank17" => "", //
                        "blank18" => "", //
                        "blank19" => "", //
                        "blank20" => "", //
                        "blank21" => "", //
                        "blank22" => "", //
                        "blank23" => "", //
                        "blank24" => "", //
                        "blank25" => "", //
                        "blank26" => "", //
                        "blank27" => "", //
                        "blank28" => "", //
                        "blank30" => "", //
                        "blank31" => "", //
                        "blank32" => "", //
                        "blank33" => "", //
                        "blank34" => "", //
                        "address2" => $value["address1"], // 43
                        "blank35" => "", // ito
                        "blank36" => "", //
                        "city2" => $value["city"], // ito
                        "blank37" => "", //
                        "region2" => $value["region"], // ito
                        "country2" => $value["country"], // ito
                        "blank38" => "", //
                        "blank39" => "", //
                        "blank40" => "", //
                        "blank41" => "", //
                        "blank42" => "", //
                        "blank43" => "", //
                        "blank44" => "", //
                        "blank45" => "", //
                        "blank46" => "", //
                        "blank47" => "", //
                        "blank48" => "", //
                        "blank49" => "", //
                        "blank50" => "", //
                        "blank51" => "", //
                        "blank52" => "", //
                        "blank53" => "", //
                        "blank54" => "", //
                        "blank55" => "", //
                        "blank56" => "", //
                        "blank57" => "", //
                        "blank58" => "", //
                        "blank59" => "", //
                        "blank60" => "", //
                        "blank61" => "", //
                        "blank62" => "", //
                        "blank63" => "", //
                        "address3" => $value["address1"], // 76
                        "blank64" => "", //
                        "blank65" => "", //
                        "city3" => $value["city"], // 79
                        "blank66" => "", //
                        "region3" => $value["region"], // 79
                        "country3" => $value["country"], // 79
                        "blank" => "", //
                        "blank" => "", //
                        "blank" => "", //
                        "blank" => "", //
                        "blank" => "", //
                        "blank" => "", //
                        "blank" => "", //
                        "blank" => "", //
                        "blank" => "", //
                        "blank" => "", //
                        "blank" => "", //
                        "blank" => "", //
                        "blank" => "", //
                        "blank" => "", //
                        "blank" => "", //
                        "blank" => "", //
                        "blank" => "", //
                        "blank" => "", //
                        "blank" => "", //
                        "blank" => "", //
                        "blank" => "", //
                        "blank" => "", //
                        "blank" => "", //
                        "blank" => "", //
                        "blank" => "", //
                        "blank" => "", //
                        "blank" => "", //
                        "blank" => "", //
                        "blank" => "", //
                        "blank" => "", //
                        "blank" => "", //
                        "blank" => "", //
                        "blank" => "", //
                        "blank" => "", //
                        "blank" => "", //
                        "blank" => "", //
                        "blank" => "", //
                        
                    )
                );

                foreach ($arr as $key => $value)
                {
                    if($exporttype == "pipe"){
                        fputcsv($file, $value, '|');
                    }else{
                        fputcsv($file, $value);
                    } 
                }
        }

        }
        else if ($type == "Contact")
        {

            $header = array(
                "Contact ID", "First Name", "Title", "Last Name", "Middle Name", "Active", "Business Account", "Owner", "Workgroup", "Job Title", "Occupation", 
                "Contact Class", "PWD", "Senior Citizen", "Zero Rated", "Company Name", "Email", "Web", "Business 1", "Cell", "Home", "Business Fax", "Address Line 1", 
                "Address Line 2", "Baranggay", "City", "State/Province", "Region", "Country", "PSGC", "Postal Code", "Contact Method", "Do Not Call", "Do Not Email", 
                "No Mass Mail", "Do Not Fax", "Do Not Mail", "No Marketing", "Parent Account", "Birthday", "Gender", "Marital Status", "Spouse/Partner Name"
            );
            fputcsv($file, $header);
            $array = [];
            $arr = [];
            foreach ($customer_data->result_array() as $value)
            {

                $arr = array(
                    $array = array(
                        "id" => $value["id"],
                        "fname" => $value["fname"],
                        "title" => $value["title"],
                        "lname" => $value['lname'],
                        "mname" => $value['mname'],
                        "blank3" => "",
                        "blank4" => "",
                        "blank5" => "",
                        "blank6" => "",
                        "blank7" => "",
                        "blank8" => "",
                        "blank9" => "",
                        "blank10" => "",
                        "blank11" => "",
                        "blank12" => "",
                        "blank13" => "", //
                        "blank14" => "", //
                        "blank15" => "", //
                        "blank16" => "", //
                        "blank17" => "", //
                        "blank18" => "", //
                        "blank19" => "", //
                        "address1" => $value["address1"],
                        "blank20" => "", //
                        "blank21" => "", //
                        "city" => $value["city"],
                        "blank22" => "",
                        "region" => $value["region"],
                        "country" => $value["country"],
                        "blank23" => "", //
                        "blank24" => "", //
                        "blank25" => "", //
                        "blank26" => "", //
                        "blank27" => "", //
                        "blank28" => "", //
                        "blank30" => "", //
                        "blank31" => "", //
                        "blank32" => "", //
                        "blank33" => "", //
                        "birthday" => $value["birthday"],
                        "blank34" => "", //
                        "blank38" => "", //
                        "blank39" => "", //
                        "blank40" => "", //
                        
                    )
                );

                foreach ($arr as $key => $value)
                {
                    if($exporttype == "pipe"){
                        fputcsv($file, $value, '|');
                    }else{
                        fputcsv($file, $value);
                    } 
                }
            }

        }
        else
        {
            $header = array("Field Name","Customer ID","Customer Name","Status","Company Name","Attention","Email","Phone 1","Phone 2","Fax",
            "Account Ref #","Address Line 1","Address Line 2","Barangay","City","State/Province","Region","Country","PSGC","Postal Code",
            "Customer Class","Terms","Statement Cycle ID","Auto-Apply Payments","Apply Overdue Charges","Enable Write-offs","Write-off Limit",
            "Currency ID","Enable Currency Override","Curr. Rate Type","Enable Rate Override","Credit Verification",
            "Consented to the Processing of Personal Data","Date of Consent","Consent Expires","Sames as Main","Company Name","Attention",
            "Phone 1","Phone 2","Fax","Email","Web","Address Line 1","Address Line 2","Baranggay","City","State/Province","Region","Country",
            "PSGC","Postal Code","Allow Appointment Notification","Require Customer Signature Mobile App","Default Billing Customer",
            "Parent Account","Consolidate Balance","Consolidate Statements","Share Credit Policy","Send Invoice by Email",
            "Send Dunning Letters by Email","Send Statements by Email","Print Invoices","Print Dunning Letters","Print Statements",
            "Statement Type","Multi-Currency Statements","Same as Main","Company Name","Attention","Email","Web","Phone 1","Phone 2",
            "Fax","Same as Main","Address Line 1","Address Line 2","Baranggay","City","State/Province","Region","Country","PSGC","Postal Code",
            "Location Name","Tax Registration ID","Tax Zone","Entity Usage Type","Shipping Branch","Price Class","Warehouse","Ship Via",
            "Shipping Terms","Shipping Zone ID","FOB Point","Resident Delivery","Saturday Delivery","Insurance","Shipping Rule","Order Priority",
            "Lead Time (days)","AR Account","AR Sub","Sales Account","Sales Sub","Discount Sub","Freight Account","Freight Sub",
            "Cash Discount Account","Cash Discount Sub","Prepayment Account","Prepayment Sub"
            );
            fputcsv($file, $header);
            $array = [];
            $arr = [];
            foreach ($customer_data->result_array() as $value)
            {

                $arr = array(
                    $array = array(
                        "fieldname" => "",
                        "id" => "",
                        "fname" => $value["customerName"],
                        "title" => $value["status"],
                        "blank3" => "",
                        "blank4" => "",
                        "blank5" => "",
                        "blank6" => "",
                        "blank7" => "",
                        "blank8" => "",
                        "blank9" => "",
                        "address1" => $value["address1"],
                        "blank10" => "",
                        "blank11" => "",
                        "city" => $value["city"],
                        "a" => "",
                        "region" => $value["region"],
                        "country" => $value["country"],
                        "blank12" => "",
                        "blank13" => "", //
                        "class" => $value["class"],
                        "blank14" => "", //
                        "blank15" => "", //
                        "blank16" => "", //
                        "blank17" => "", //
                        "blank18" => "", //
                        "blank19" => "", //
                        "blank20" => "", //
                        "blank21" => "", //
                        "blank23" => "", //
                        "blank24" => "", //
                        "blank25" => "", //
                        "blank26" => "", //
                        "blank27" => "", //
                        "blank28" => "", //
                        "blank30" => "", //
                        "blank31" => "", //
                        "blank32" => "", //
                        "blank33" => "", //
                        "blank34" => "", //
                        "blank22" => "", //
                        "blank38" => "", //
                        "blank39" => "", //
                        "address2" => $value["address1"],
                        "blank40" => "", //
                        "blank41" => "", //
                        "city2" => $value["city"],
                        "blank42" => "", //
                        "region2" => $value["region"],
                        "country2" => $value["country"],
                        "blank43" => "", //
                        "blank44" => "", //
                        "blank45" => "", //
                        "blank46" => "", //
                        "blank47" => "", //
                        "blank48" => "", //
                        "blank49" => "", //
                        "blank50" => "", //
                        "blank51" => "", //
                        "blank52" => "", //
                        "blank53" => "", //
                        "blank54" => "", //
                        "blank55" => "", //
                        "blank56" => "", //
                        "blank57" => "", //
                        "blank58" => "", //
                        "blank59" => "", //
                        "blank60" => "", //
                        "blank61" => "", //
                        "blank62" => "", //
                        "blank63" => "", //
                        "blank95" => "", //
                        "blank96" => "", //
                        "blank97" => "", //
                        "blank98" => "", //
                        "blank99" => "", //
                        "blank100" => "", //
                        "address2" => $value["address1"],
                        "blank64" => "", //
                        "blank65" => "", //
                        "city2" => $value["city"],
                        "blank66" => "", //
                        "region2" => $value["region"],
                        "country2" => $value["country"],
                        "blank94" => "", //
                        "blank67" => "", //
                        "blank68" => "", // 
                        "blank69" => "", //
                        "blank70" => "", //
                        "blank71" => "", //
                        "tax" => $value["tax"],
                        "blank72" => "", //
                        "blank73" => "", //
                        "blank74" => "", //
                        "blank75" => "", //
                        "blank76" => "", //
                        "blank77" => "", //
                        "blank78" => "", //
                        "blank79" => "", //
                        "blank80" => "", //
                        "blank81" => "", //
                        "blank82" => "", //
                        "blank83" => "", //
                        "blank84" => "", //
                        "blank85" => "", //
                        "blank86" => "", //
                        "blank87" => "", //
                        "blank88" => "", //
                        "blank89" => "", //
                        "blank90" => "", //
                        "blank91" => "", //
                        "blank92" => "", //
                        "blank93" => "", //
                        
                    )
                );

                foreach ($arr as $key => $value)
                {
                    if($exporttype == "pipe"){
                        fputcsv($file, $value, '|');
                    }else{
                        fputcsv($file, $value);
                    } 
                }
            }
        }

        fclose($file);
        exit;
    }

    function import()
    {
        // Check form submit or not
        $reg_num = $this->input->post('range');

        if ($this->input->post('upload') != NULL)
        {
            $data = array();

            if (!empty($_FILES['file']['name']))
            {
                // Set preference
                $config['upload_path'] = 'assets/files/';
                $config['allowed_types'] = 'csv';
                $config['max_size'] = '100000'; // max_size in kb
                $config['file_name'] = $_FILES['file']['name'];

                // Load upload library
                $this->load->library('upload', $config);

                // File upload
                if ($this->upload->do_upload('file'))
                {
                    // Get data about the file
                    $uploadData = $this->upload->data();
                    $filename = $uploadData['file_name'];

                    // Reading file
                    $file = fopen("assets/files/" . $filename, "r");
                    $i = 0;
                    $numberOfFields = 31; // Total number of fields
                    $importData_arr = array();

                    while (($filedata = fgetcsv($file, 100000, ",")) !== false)
                    {
                        $num = count($filedata);

                        if ($numberOfFields == $num)
                        {
                            for ($c = 0;$c < $num;$c++)
                            {
                                $importData_arr[$i][] = $filedata[$c];
                            }
                        }
                        $i++;
                    }
                    fclose($file);

                    $skip = 0;

                    // insert import data
                    foreach ($importData_arr as $record)
                    {

                        // Skip first row
                        if ($skip != 0)
                        {

                            if(count($record) > 0){       
                
                                //if(preg_match('/^\d{2}\/\d{2}\/\d{2}$/', $record[17]) || preg_match('([0-9]{2}\/[0-9]{2}\/[0-9]{4})', $record[17])){ // validate birthday
                                    $class = substr($record[0], -4, 1); 
                    
                                    $data = array( //get company and name
                                        "name" => $record[1],
                                        "title" => $record[19],
                                        //"tax" => $record[2],
                                    );
                                    
                                    $fname = "";
                                    $mname = "";
                                    //$tax = "";
                                    $lname = "";

                                    if(preg_match('/^[a-zA-Z]/', $record[1])){
                    
                                    if($data["title"] == "Mr." || $data["title"] == "Ms."){ // for individual only
                    
                                        $lname = explode(',',trim($data['name'])); //get last name
                                        $name = explode(' ',trim($data['name'])); //get first and middle name

                                        //$response = $this->data_model->selectRecord('lname', $lname['0'], 'birthday', $record[17]); //check individual dup

                                        // handling empty values 
                                        if(!empty($name[2])) { 
                                            $mname = $name['2']; 
                                            if(!empty($name[3])) { 
                                                $mname = $mname . ' ' . $name['3']; 
                                                if(!empty($name[4])) { 
                                                    $mname = $mname . ' ' .$name['4']; 
                                                    if(!empty($name[5])) { 
                                                        $mname = $mname . ' ' .$name['5']; 
                                                        if(!empty($name[6])) { 
                                                            $mname = $mname . ' ' .$name['6']; 
                                                        } 
                                                    } 
                                                } 
                                                
                                            } 
                                        } 
                                        if(!empty($name[2])) { //not empty first name
                                            $fname = $name['1']; 
                                        } 
                    
                                        if(!empty($lname[0])){ //not empty last name
                                            $lname = $lname['0'];
                                        }
                                    
                                    }
                                    
                                }
                                    
                                    // else{
                                    //     $tax = $data['tax'];
                                    //     //$response = $this->data_model->selectRecord('customerName', $record[1], 'tax', $tax); //check company dup
                                    // }        
                                    
                                    // Insert record
                                    //if(count($response) == 0){
                                    $newcustomer = array(  
                                        array(
                                            "customerName" => $record[1],
                                            "fname" => $fname,
                                            "mname" => $mname,
                                            "lname" => $lname,
                                            "address1" => $record[6],
                                            "city" => $record[4],
                                            "region" => $record[9],
                                            "class" => $class,
                                            "birthday" => $record[17],
                                            "title" => $record[19],
                                            "tax" => $record[2]
                                        )
                                    );

                                       // $customer = [];
                    
                                        // if(preg_match('/^[0-9]-[0-9]$/', $reg_num)){
                                        //     $reg_num = explode("-", $reg_num);
                        
                                        //     $azRange = range($reg_num[0], $reg_num[1]);
                        
                                        //         foreach ($azRange as $reg_num)
                                        //         {
                                        //             if($newcustomer[0]["region"] == $reg_num){
                                        //                 //$customer = $newcustomer;
                                        //                 $response = $this->data_model->insertRecord($newcustomer);        
                                        //             }
                                        //         }
                                        // }else{
                    
                                        //     if($newcustomer[0]["region"] == $reg_num){
                                                //$customer = $newcustomer;
                                                $response = $this->data_model->insertRecord($newcustomer);  
                                        //     }
                                        // }  
                                    //}  
                                //}
                            } 
                        }
                        $skip++;
                    }
                    $data['response'] = 'successfully uploaded ' . $filename;
                }
                else
                {
                    $data['response'] = 'failed';
                }
            }
            else
            {
                $data['response'] = 'failed';
            }
            // load view
            $this->index();
        }
        else
        {
            // load view
            $this->index();
        }
    }

}

