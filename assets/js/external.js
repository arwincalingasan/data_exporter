
$(document).ready(function(){
    var base_url = '<?php echo base_url();?>'
 $("#importbtn").click(function(){
  var search = $('#search').val();

  $.ajax({
   url: base_url + 'index.php/data/import',
   type: 'post',
   data: {search:search},
   beforeSend: function(){
    // Show image container
    $("#loader").show();
   },
   success: function(response){
    $('.response').empty();
    $('.response').append(response);
   },
   complete:function(data){
    // Hide image container
    $("#loader").hide();
   }
  });
 
 });
});
